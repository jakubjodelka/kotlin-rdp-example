package jodelka.me.marvelcharacters.viper.entity

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by jjodelka on 24/01/2017.
 */
open class Character : RealmObject() {

    @PrimaryKey var id: Int = 0
    var name: String? = null
    var description: String? = null
    var thumbnail: Thumbnail? = null
    var resourceUrl: String? = null
    var urls: RealmList<Urls>? = null
    var comics: Comics? = null
    var series: Series? = null

}

open class Urls : RealmObject() {

    var type: String? = null
    var url: String? = null

    override fun toString(): String {
        return "Urls(type=$type, url=$url)"
    }

}

open class Thumbnail : RealmObject() {

    var path: String? = null
    var extension: String? = null

    override fun toString(): String {
        return "Thumbnail(path=$path, extension=$extension)"
    }

}

open class Comics : RealmObject() {

    var items: RealmList<SimpleResponseItem>? = null

}

open class Series : RealmObject() {

    var items: RealmList<SimpleResponseItem>? = null

}

open class SimpleResponseItem : RealmObject() {

    var resourceURI: String? = null
    var name: String? = null
    var type: String? = null

    override fun toString(): String {
        return "SimpleResponseItem(resourceURI=$resourceURI, name=$name, type=$type)"
    }

}


