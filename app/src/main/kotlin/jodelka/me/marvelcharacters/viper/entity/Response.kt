package jodelka.me.marvelcharacters.viper.entity

/**
 * Created by jjodelka on 28/01/2017.
 */

data class Response<T>(val code: String,
                       val etag: String,
                       val data: Data<T>)

data class Data<T>(val offset: Int,
                   val limit: Int,
                   val total: Int,
                   val count: Int,
                   val results: Array<T>)
