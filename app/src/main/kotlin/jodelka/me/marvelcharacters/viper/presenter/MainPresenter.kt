package jodelka.me.marvelcharacters.viper.presenter

import com.mateuszkoslacz.moviper.base.presenter.BaseRxPresenter

import jodelka.me.marvelcharacters.viper.contract.MainContract
import jodelka.me.marvelcharacters.viper.routing.MainRouting
import jodelka.me.marvelcharacters.viper.interactor.MainInteractor

class MainPresenter : BaseRxPresenter<MainContract.View, MainContract.Interactor, MainContract.Routing>(), MainContract.Presenter {

    override fun createRouting(): MainContract.Routing {
        return MainRouting()
    }

    override fun createInteractor(): MainContract.Interactor {
        return MainInteractor()
    }
}
