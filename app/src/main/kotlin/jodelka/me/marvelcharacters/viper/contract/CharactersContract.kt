package jodelka.me.marvelcharacters.viper.contract

import com.hannesdorfmann.mosby.mvp.MvpView
import com.mateuszkoslacz.moviper.iface.presenter.ViperPresenter
import com.mateuszkoslacz.moviper.iface.interactor.ViperRxInteractor
import com.mateuszkoslacz.moviper.iface.routing.ViperViewHelperRxRouting
import com.mateuszkoslacz.moviper.iface.viewhelper.ViperViewHelper

interface CharactersContract {

    interface Presenter : ViperPresenter<View>

    interface View : MvpView

    interface Interactor : ViperRxInteractor

    interface Routing : ViperViewHelperRxRouting<ViewHelper>

    interface ViewHelper : ViperViewHelper
}

