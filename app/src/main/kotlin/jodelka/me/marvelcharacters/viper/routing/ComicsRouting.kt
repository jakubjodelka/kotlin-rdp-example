package jodelka.me.marvelcharacters.viper.routing

import com.mateuszkoslacz.moviper.base.routing.BaseViewHelperRxRouting

import jodelka.me.marvelcharacters.viper.contract.ComicsContract

class ComicsRouting : BaseViewHelperRxRouting<ComicsContract.ViewHelper>(), ComicsContract.Routing
