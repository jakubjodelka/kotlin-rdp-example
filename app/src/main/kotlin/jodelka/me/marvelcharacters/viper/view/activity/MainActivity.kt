package jodelka.me.marvelcharacters.viper.view.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.WindowManager
import com.mateuszkoslacz.moviper.base.view.activity.ViperActivity
import jodelka.me.marvelcharacters.R
import jodelka.me.marvelcharacters.viper.contract.MainContract
import jodelka.me.marvelcharacters.viper.presenter.MainPresenter
import jodelka.me.marvelcharacters.viper.view.adapter.TabPagerAdapter
import jodelka.me.marvelcharacters.viper.view.fragment.CharactersFragment
import jodelka.me.marvelcharacters.viper.view.fragment.ComicsFragment
import jodelka.me.marvelcharacters.viper.view.fragment.CreatorsFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : ViperActivity<MainContract.View, MainContract.Presenter>(), MainContract.View {

    private var mImageArray: IntArray = intArrayOf()
    private var mColorArray: IntArray = intArrayOf()
    private var mTitlesArray: Array<String> = arrayOf()

    private var mFragmentsArray: Array<Fragment> = arrayOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        initFragments()
        configureTabs()
        initViewPager()

        activity.coordinatorTabLayout
                .setImageArray(mImageArray, mColorArray)
                .setupWithViewPager(activity.viewPager)
    }

    private fun configureTabs() {
        mImageArray = intArrayOf(R.mipmap.characters,
                R.mipmap.comics,
                R.mipmap.creators)
        mColorArray = intArrayOf(R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorPrimary)
        mTitlesArray = arrayOf("Characters",
                "Comics",
                "Creators")
    }

    private fun initFragments() {
        mFragmentsArray = arrayOf(CharactersFragment(), ComicsFragment(), CreatorsFragment())
    }

    private fun initViewPager() {
        activity.viewPager.offscreenPageLimit = 3
        activity.viewPager.adapter =
                TabPagerAdapter(supportFragmentManager, mFragmentsArray, mTitlesArray)
    }

    override fun createPresenter(): MainContract.Presenter {
        return MainPresenter()
    }

}
