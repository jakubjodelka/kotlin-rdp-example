package jodelka.me.marvelcharacters.viper.contract

import com.hannesdorfmann.mosby.mvp.MvpView
import com.mateuszkoslacz.moviper.iface.presenter.ViperPresenter
import com.mateuszkoslacz.moviper.iface.interactor.ViperRxInteractor
import com.mateuszkoslacz.moviper.iface.view.ViperView
import com.mateuszkoslacz.moviper.iface.routing.ViperRxRouting

interface MainContract {

    interface Presenter : ViperPresenter<View>

    interface View : MvpView

    interface Interactor : ViperRxInteractor

    interface Routing : ViperRxRouting
}
