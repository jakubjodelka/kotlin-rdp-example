package jodelka.me.marvelcharacters.viper.interactor

import com.mateuszkoslacz.moviper.base.interactor.BaseRxInteractor

import jodelka.me.marvelcharacters.viper.contract.CharactersContract

class CharactersInteractor : BaseRxInteractor(), CharactersContract.Interactor
