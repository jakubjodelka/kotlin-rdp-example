package jodelka.me.marvelcharacters.viper.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import jodelka.me.marvelcharacters.R
import jodelka.me.marvelcharacters.viper.contract.ComicsContract
import jodelka.me.marvelcharacters.viper.presenter.ComicsPresenter

import com.mateuszkoslacz.moviper.base.view.fragment.ViperFragment

class ComicsFragment : ViperFragment<ComicsContract.View, ComicsContract.Presenter>(),
        ComicsContract.View,
        ComicsContract.ViewHelper {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_comics, container, false)
    }

    override fun createPresenter(): ComicsContract.Presenter {
        return ComicsPresenter()
    }

}
