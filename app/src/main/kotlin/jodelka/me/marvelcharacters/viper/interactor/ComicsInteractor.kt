package jodelka.me.marvelcharacters.viper.interactor

import com.mateuszkoslacz.moviper.base.interactor.BaseRxInteractor

import jodelka.me.marvelcharacters.viper.contract.ComicsContract

class ComicsInteractor : BaseRxInteractor(), ComicsContract.Interactor
