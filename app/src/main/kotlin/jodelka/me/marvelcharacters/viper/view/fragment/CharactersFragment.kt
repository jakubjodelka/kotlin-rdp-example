package jodelka.me.marvelcharacters.viper.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mateuszkoslacz.moviper.base.view.fragment.ViperFragment
import jodelka.me.marvelcharacters.R
import jodelka.me.marvelcharacters.viper.contract.CharactersContract
import jodelka.me.marvelcharacters.viper.presenter.CharactersPresenter

class CharactersFragment : ViperFragment<CharactersContract.View, CharactersContract.Presenter>(),
        CharactersContract.View,
        CharactersContract.ViewHelper {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_characters, container, false)
    }

    override fun createPresenter(): CharactersContract.Presenter {
        return CharactersPresenter()
    }

}
