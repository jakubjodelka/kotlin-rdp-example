package jodelka.me.marvelcharacters.viper.routing

import com.mateuszkoslacz.moviper.base.routing.BaseViewHelperRxRouting

import jodelka.me.marvelcharacters.viper.contract.CharactersContract

class CharactersRouting : BaseViewHelperRxRouting<CharactersContract.ViewHelper>(), CharactersContract.Routing
