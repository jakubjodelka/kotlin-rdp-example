package jodelka.me.marvelcharacters.viper.routing

import com.mateuszkoslacz.moviper.base.routing.BaseViewHelperRxRouting

import jodelka.me.marvelcharacters.viper.contract.CreatorsContract

class CreatorsRouting : BaseViewHelperRxRouting<CreatorsContract.ViewHelper>(), CreatorsContract.Routing
