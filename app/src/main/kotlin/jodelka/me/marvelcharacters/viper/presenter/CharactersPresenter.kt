package jodelka.me.marvelcharacters.viper.presenter

import com.mateuszkoslacz.moviper.base.presenter.BaseRxPresenter

import jodelka.me.marvelcharacters.viper.contract.CharactersContract
import jodelka.me.marvelcharacters.viper.routing.CharactersRouting
import jodelka.me.marvelcharacters.viper.interactor.CharactersInteractor

class CharactersPresenter : BaseRxPresenter<CharactersContract.View, CharactersContract.Interactor, CharactersContract.Routing>(), CharactersContract.Presenter {

    override fun createRouting(): CharactersContract.Routing {
        return CharactersRouting()
    }

    override fun createInteractor(): CharactersContract.Interactor {
        return CharactersInteractor()
    }
}
