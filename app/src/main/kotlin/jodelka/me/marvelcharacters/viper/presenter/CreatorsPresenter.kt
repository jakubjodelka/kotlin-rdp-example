package jodelka.me.marvelcharacters.viper.presenter

import com.mateuszkoslacz.moviper.base.presenter.BaseRxPresenter

import jodelka.me.marvelcharacters.viper.contract.CreatorsContract
import jodelka.me.marvelcharacters.viper.routing.CreatorsRouting
import jodelka.me.marvelcharacters.viper.interactor.CreatorsInteractor

class CreatorsPresenter : BaseRxPresenter<CreatorsContract.View, CreatorsContract.Interactor, CreatorsContract.Routing>(), CreatorsContract.Presenter {

    override fun createRouting(): CreatorsContract.Routing {
        return CreatorsRouting()
    }

    override fun createInteractor(): CreatorsContract.Interactor {
        return CreatorsInteractor()
    }
}
