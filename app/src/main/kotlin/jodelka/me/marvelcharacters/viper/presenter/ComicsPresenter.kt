package jodelka.me.marvelcharacters.viper.presenter

import com.mateuszkoslacz.moviper.base.presenter.BaseRxPresenter

import jodelka.me.marvelcharacters.viper.contract.ComicsContract
import jodelka.me.marvelcharacters.viper.routing.ComicsRouting
import jodelka.me.marvelcharacters.viper.interactor.ComicsInteractor

class ComicsPresenter : BaseRxPresenter<ComicsContract.View, ComicsContract.Interactor, ComicsContract.Routing>(), ComicsContract.Presenter {

    override fun createRouting(): ComicsContract.Routing {
        return ComicsRouting()
    }

    override fun createInteractor(): ComicsContract.Interactor {
        return ComicsInteractor()
    }
}
