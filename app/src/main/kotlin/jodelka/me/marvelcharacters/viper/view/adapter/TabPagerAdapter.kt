package jodelka.me.marvelcharacters.viper.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by jjodelka on 05/02/2017.
 */

class TabPagerAdapter(fragmentManager: FragmentManager,
                      val mFragments: Array<Fragment>,
                      val mTitles: Array<String>)
    : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return mFragments.get(position)
    }

    override fun getCount(): Int {
        return mFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return mTitles[position]
    }

}