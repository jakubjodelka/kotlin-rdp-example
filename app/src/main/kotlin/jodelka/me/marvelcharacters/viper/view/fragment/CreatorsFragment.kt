package jodelka.me.marvelcharacters.viper.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import jodelka.me.marvelcharacters.R
import jodelka.me.marvelcharacters.viper.contract.CreatorsContract
import jodelka.me.marvelcharacters.viper.presenter.CreatorsPresenter

import com.mateuszkoslacz.moviper.base.view.fragment.ViperFragment

class CreatorsFragment : ViperFragment<CreatorsContract.View, CreatorsContract.Presenter>(),
        CreatorsContract.View,
        CreatorsContract.ViewHelper {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_creators, container, false)
    }

    override fun createPresenter(): CreatorsContract.Presenter {
        return CreatorsPresenter()
    }

}
