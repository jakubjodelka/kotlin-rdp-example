package jodelka.me.marvelcharacters.viper.routing

import com.mateuszkoslacz.moviper.base.routing.BaseRxRouting

import jodelka.me.marvelcharacters.viper.contract.MainContract

class MainRouting : BaseRxRouting(), MainContract.Routing
