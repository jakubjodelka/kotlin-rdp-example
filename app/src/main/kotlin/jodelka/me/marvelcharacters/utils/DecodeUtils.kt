package jodelka.me.marvelcharacters.utils

import java.math.BigInteger
import java.security.MessageDigest

/**
 * Created by jjodelka on 24/01/2017.
 */

object DecodeUtils {

    fun hashMD5(message: String): String {
        val m = MessageDigest.getInstance("MD5")
        m.reset()
        m.update(message.toByteArray())
        var hashtext = BigInteger(1, m.digest()).toString(16)
        while (hashtext.length < 32) {
            hashtext = "0" + hashtext
        }
        return hashtext
    }
}