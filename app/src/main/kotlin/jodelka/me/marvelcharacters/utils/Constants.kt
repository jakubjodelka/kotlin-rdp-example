package jodelka.me.marvelcharacters.utils

import jodelka.me.marvelcharacters.BuildConfig

/**
 * Created by jjodelka on 23/01/2017.
 */

object Constants {

    var MARVEL_API_URL = "https://gateway.marvel.com/v1/public/"

    var API_KEY_PUBLIC = BuildConfig.MARVEL_API_KEY_PUBLIC
    var API_KEY_PRIVATE = BuildConfig.MARVEL_API_KEY_PRIVATE

}