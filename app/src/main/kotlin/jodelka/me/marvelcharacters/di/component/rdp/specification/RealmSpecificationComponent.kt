package jodelka.me.marvelcharacters.di.component.rdp.specification

import dagger.Component
import jodelka.me.marvelcharacters.data.rdp.specification.realm.impl.AllCharactersRealmSpecification
import jodelka.me.marvelcharacters.di.module.rdp.specification.realm.AllCharactersRealmSpecificationModule
import javax.inject.Singleton

/**
 * Created by jjodelka on 29/01/2017.
 */

@Singleton
@Component(modules = arrayOf(AllCharactersRealmSpecificationModule::class))
interface RealmSpecificationComponent {

    fun provideAllCharactersSpecification(): AllCharactersRealmSpecification
}