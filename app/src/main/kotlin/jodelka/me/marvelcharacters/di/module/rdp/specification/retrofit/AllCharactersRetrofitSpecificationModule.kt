package jodelka.me.marvelcharacters.di.module.rdp.specification.retrofit

import dagger.Module
import dagger.Provides
import jodelka.me.marvelcharacters.data.rdp.specification.retrofit.impl.AllCharactersRetrofitSpecification

/**
 * Created by jjodelka on 29/01/2017.
 */

@Module
class AllCharactersRetrofitSpecificationModule {

    @Provides
    fun provideAllCharactersRetrofitSpecification(): AllCharactersRetrofitSpecification {
        return AllCharactersRetrofitSpecification()
    }
}