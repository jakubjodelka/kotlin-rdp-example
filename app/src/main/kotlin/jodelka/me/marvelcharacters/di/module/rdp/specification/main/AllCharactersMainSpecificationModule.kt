package jodelka.me.marvelcharacters.di.module.rdp.specification.main

import dagger.Module
import dagger.Provides
import jodelka.me.marvelcharacters.data.rdp.specification.main.impl.AllCharactersMainSpecification

/**
 * Created by jjodelka on 29/01/2017.
 */

@Module
class AllCharactersMainSpecificationModule {

    @Provides
    fun provideAllCharactersMainSpecification(): AllCharactersMainSpecification {
        return AllCharactersMainSpecification()
    }
}