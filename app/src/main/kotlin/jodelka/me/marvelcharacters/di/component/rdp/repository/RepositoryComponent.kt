package jodelka.me.marvelcharacters.di.component.rdp.repository

import dagger.Component
import jodelka.me.marvelcharacters.data.rdp.repository.main.MainCharacterRepository
import jodelka.me.marvelcharacters.data.rdp.repository.realm.RealmCharacterRepository
import jodelka.me.marvelcharacters.data.rdp.repository.retrofit.RetrofitCharacterRepository
import jodelka.me.marvelcharacters.di.module.rdp.repository.MainCharacterRepositoryModule
import jodelka.me.marvelcharacters.di.module.rdp.repository.RealmCharacterRepositoryModule
import jodelka.me.marvelcharacters.di.module.rdp.repository.RetrofitCharacterRepositoryModule
import javax.inject.Singleton

/**
 * Created by jjodelka on 29/01/2017.
 */

@Singleton
@Component(modules = arrayOf(MainCharacterRepositoryModule::class,
        RetrofitCharacterRepositoryModule::class,
        RealmCharacterRepositoryModule::class))
interface RepositoryComponent {

    fun provideMainCharacterRepository(): MainCharacterRepository

    fun provideRealmCharacterRepository(): RealmCharacterRepository

    fun provideRetrofitCharacterRepository(): RetrofitCharacterRepository
}