package jodelka.me.marvelcharacters.di

import android.content.Context
import jodelka.me.marvelcharacters.di.component.rdp.repository.DaggerRepositoryComponent
import jodelka.me.marvelcharacters.di.component.rdp.repository.RepositoryComponent
import jodelka.me.marvelcharacters.di.component.rdp.specification.*
import jodelka.me.marvelcharacters.di.module.rdp.repository.MainCharacterRepositoryModule
import jodelka.me.marvelcharacters.di.module.rdp.repository.RealmCharacterRepositoryModule
import jodelka.me.marvelcharacters.di.module.rdp.repository.RetrofitCharacterRepositoryModule
import jodelka.me.marvelcharacters.di.module.rdp.specification.main.AllCharactersMainSpecificationModule
import jodelka.me.marvelcharacters.di.module.rdp.specification.realm.AllCharactersRealmSpecificationModule
import jodelka.me.marvelcharacters.di.module.rdp.specification.retrofit.AllCharactersRetrofitSpecificationModule

/**
 * Created by jjodelka on 29/01/2017.
 */

object DIProvider {

    private var context: Context? = null

    fun init(context: Context) {
        this.context = context
    }

    var repositoryComponent: RepositoryComponent? = null
        get() {
            assertNotNullContext()
            if (repositoryComponent == null) {
                repositoryComponent = DaggerRepositoryComponent.builder()
                        .mainCharacterRepositoryModule(MainCharacterRepositoryModule())
                        .realmCharacterRepositoryModule(RealmCharacterRepositoryModule())
                        .retrofitCharacterRepositoryModule(RetrofitCharacterRepositoryModule())
                        .build()
            }
            return repositoryComponent
        }

    var mainSpecifcationComponent: MainSpecificationComponent? = null
        get() {
            assertNotNullContext()
            if (mainSpecifcationComponent == null) {
                mainSpecifcationComponent = DaggerMainSpecificationComponent.builder()
                        .allCharactersMainSpecificationModule(AllCharactersMainSpecificationModule())
                        .build()
            }
            return mainSpecifcationComponent
        }

    var realmSpecificationComponent: RealmSpecificationComponent? = null
        get() {
            assertNotNullContext()
            if (realmSpecificationComponent == null) {
                realmSpecificationComponent = DaggerRealmSpecificationComponent.builder()
                        .allCharactersRealmSpecificationModule(AllCharactersRealmSpecificationModule())
                        .build()
            }
            return realmSpecificationComponent
        }

    var retrofitSpecificationComponent: RetrofitSpecificationComponent? = null
        get() {
            assertNotNullContext()
            if (retrofitSpecificationComponent == null) {
                retrofitSpecificationComponent = DaggerRetrofitSpecificationComponent.builder()
                        .allCharactersRetrofitSpecificationModule(AllCharactersRetrofitSpecificationModule())
                        .build()
            }
            return retrofitSpecificationComponent
        }

    private fun assertNotNullContext() {
        if (context == null) {
            throw IllegalStateException("You have to init DIProvider with context first")
        }
    }
}