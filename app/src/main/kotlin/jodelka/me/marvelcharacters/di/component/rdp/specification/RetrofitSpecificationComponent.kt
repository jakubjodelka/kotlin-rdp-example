package jodelka.me.marvelcharacters.di.component.rdp.specification

import dagger.Component
import jodelka.me.marvelcharacters.data.rdp.specification.retrofit.impl.AllCharactersRetrofitSpecification
import jodelka.me.marvelcharacters.di.module.rdp.specification.retrofit.AllCharactersRetrofitSpecificationModule
import javax.inject.Singleton

/**
 * Created by jjodelka on 29/01/2017.
 */

@Singleton
@Component(modules = arrayOf(AllCharactersRetrofitSpecificationModule::class))
interface RetrofitSpecificationComponent {

    fun provideAllCharactersSpecification(): AllCharactersRetrofitSpecification
}