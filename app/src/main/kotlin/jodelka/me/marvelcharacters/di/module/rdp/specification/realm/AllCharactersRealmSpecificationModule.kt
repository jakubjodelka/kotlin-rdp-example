package jodelka.me.marvelcharacters.di.module.rdp.specification.realm

import dagger.Module
import dagger.Provides
import jodelka.me.marvelcharacters.data.rdp.specification.realm.impl.AllCharactersRealmSpecification

/**
 * Created by jjodelka on 29/01/2017.
 */

@Module
class AllCharactersRealmSpecificationModule {

    @Provides
    fun provideAllCharactersRealmSpecification(): AllCharactersRealmSpecification {
        return AllCharactersRealmSpecification()
    }
}