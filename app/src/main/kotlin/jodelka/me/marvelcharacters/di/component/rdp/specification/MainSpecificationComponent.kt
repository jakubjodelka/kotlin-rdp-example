package jodelka.me.marvelcharacters.di.component.rdp.specification

import dagger.Component
import jodelka.me.marvelcharacters.data.rdp.specification.main.impl.AllCharactersMainSpecification
import jodelka.me.marvelcharacters.di.module.rdp.specification.main.AllCharactersMainSpecificationModule
import javax.inject.Singleton

/**
 * Created by jjodelka on 29/01/2017.
 */

@Singleton
@Component(modules = arrayOf(AllCharactersMainSpecificationModule::class))
interface MainSpecificationComponent {

    fun provideAllCharacterSpecification(): AllCharactersMainSpecification
}