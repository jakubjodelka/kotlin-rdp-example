package jodelka.me.marvelcharacters.di.module.rdp.repository

import dagger.Module
import dagger.Provides
import jodelka.me.marvelcharacters.data.rdp.repository.main.MainCharacterRepository
import javax.inject.Singleton

/**
 * Created by jjodelka on 29/01/2017.
 */

@Module
class MainCharacterRepositoryModule {

    @Singleton
    @Provides
    fun provideMainCharacterRepository(): MainCharacterRepository {
        return MainCharacterRepository()
    }
}