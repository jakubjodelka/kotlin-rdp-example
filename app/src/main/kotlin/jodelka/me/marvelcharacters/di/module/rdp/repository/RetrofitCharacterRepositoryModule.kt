package jodelka.me.marvelcharacters.di.module.rdp.repository

import dagger.Module
import dagger.Provides
import jodelka.me.marvelcharacters.data.rdp.repository.retrofit.RetrofitCharacterRepository
import javax.inject.Singleton

/**
 * Created by jjodelka on 29/01/2017.
 */

@Module
class RetrofitCharacterRepositoryModule {

    @Singleton
    @Provides
    fun provideRetrofitCharacterRepository(): RetrofitCharacterRepository {
        return RetrofitCharacterRepository()
    }
}