package jodelka.me.marvelcharacters

import android.app.Application
import com.orhanobut.logger.LogLevel
import com.orhanobut.logger.Logger
import jodelka.me.marvelcharacters.di.DIProvider

/**
 * Created by jjodelka on 23/01/2017.
 */

class MarvelApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initDI()
        initLogger()
    }

    private fun initDI() {
        DIProvider.init(applicationContext)
    }

    private fun initLogger() {
        Logger.init("MARVEL")
                .methodCount(3)
                .logLevel(if (BuildConfig.DEBUG) LogLevel.FULL else LogLevel.NONE)
                .methodOffset(2)
    }
}
