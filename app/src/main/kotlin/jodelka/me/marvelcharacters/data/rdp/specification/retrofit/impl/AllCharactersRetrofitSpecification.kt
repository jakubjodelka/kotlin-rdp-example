package jodelka.me.marvelcharacters.data.rdp.specification.retrofit.impl

import jodelka.me.marvelcharacters.data.rdp.specification.base.AllCharactersSpecification
import jodelka.me.marvelcharacters.data.rdp.specification.retrofit.RetrofitStreamSpecification
import jodelka.me.marvelcharacters.utils.Constants
import jodelka.me.marvelcharacters.utils.DecodeUtils
import jodelka.me.marvelcharacters.viper.entity.Character
import jodelka.me.marvelcharacters.viper.entity.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * Created by jjodelka on 24/01/2017.
 */
class AllCharactersRetrofitSpecification(var offset: Int = 0) :
        RetrofitStreamSpecification<Character>, AllCharactersSpecification {

    private val timestamp: Long = System.currentTimeMillis()

    override fun getResults(retrofit: Retrofit): Observable<Character> {

        return retrofit.create(AllCharactersSpecificationRetrofitApiCall::class.java)
                .getCharacters(offset = offset)
                .map { it -> it.data.results }
                .flatMap { Observable.from(it) }
    }

    private interface AllCharactersSpecificationRetrofitApiCall {

        @GET("characters")
        fun getCharacters(@Query("apikey") apiKey: String = Constants.API_KEY_PUBLIC,
                          @Query("offset") offset: Int,
                          @Query("limit") limit: Int = 20,
                          @Query("ts") timeStamp: Long = timeStamp,
                          @Query("hash") hash: String = DecodeUtils.hashMD5(
                                  timeStamp.toString() +
                                          Constants.API_KEY_PRIVATE +
                                          Constants.API_KEY_PUBLIC
                          )
        ): Observable<Response<Character>>
    }
}