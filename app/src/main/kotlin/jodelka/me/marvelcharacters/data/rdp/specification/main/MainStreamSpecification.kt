package jodelka.me.marvelcharacters.data.rdp.specification.main

import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import rx.Observable

/**
 * Created by jjodelka on 29/01/2017.
 */

interface MainStreamSpecification<T> : StreamSpecification<T> {

    fun getResults(): Observable<T>
}