package jodelka.me.marvelcharacters.data.rdp.repository.retrofit

import jodelka.me.marvelcharacters.data.rdp.repository.Repository
import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import jodelka.me.marvelcharacters.data.rdp.specification.retrofit.RetrofitStreamSpecification
import jodelka.me.marvelcharacters.utils.Constants
import jodelka.me.marvelcharacters.viper.entity.Character
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable

/**
 * Created by jjodelka on 24/01/2017.
 */
class RetrofitCharacterRepository : Repository<Character> {

    private val retrofit: Retrofit
        get() {
            return Retrofit.Builder()
                    .baseUrl(Constants.MARVEL_API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build()
        }

    override fun add(item: Character) {
        throw UnsupportedOperationException("Retrofit repository is only for getting data from API")
    }

    override fun add(item: Observable<Character>) {
        throw UnsupportedOperationException("Retrofit repository is only for getting data from API")
    }

    override fun update(item: Character) {
        throw UnsupportedOperationException("Retrofit repository is only for getting data from API")
    }

    override fun remove(item: Character) {
        throw UnsupportedOperationException("Retrofit repository is only for getting data from API")
    }

    override fun streamQuery(specification: StreamSpecification<Character>): Observable<Character> {
        val retrofitSpecification: RetrofitStreamSpecification<Character>
                = specification as RetrofitStreamSpecification<Character>

        return retrofitSpecification.getResults(retrofit)
    }
}