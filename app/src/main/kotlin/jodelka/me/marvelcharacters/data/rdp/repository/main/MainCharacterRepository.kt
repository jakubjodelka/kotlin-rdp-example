package jodelka.me.marvelcharacters.data.rdp.repository.main

import jodelka.me.marvelcharacters.data.rdp.repository.Repository
import jodelka.me.marvelcharacters.data.rdp.repository.realm.RealmCharacterRepository
import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import jodelka.me.marvelcharacters.data.rdp.specification.main.MainStreamSpecification
import jodelka.me.marvelcharacters.viper.entity.Character
import rx.Observable

/**
 * Created by jjodelka on 29/01/2017.
 */

class MainCharacterRepository : Repository<Character> {

    override fun add(item: Character) {
        RealmCharacterRepository().add(item)
    }

    override fun add(item: Observable<Character>) {
        RealmCharacterRepository().add(item)
    }

    override fun update(item: Character) {
        RealmCharacterRepository().update(item)
    }

    override fun remove(item: Character) {
        RealmCharacterRepository().remove(item)
    }

    override fun streamQuery(specification: StreamSpecification<Character>): Observable<Character> {
        val mainSpecification: MainStreamSpecification<Character>
                = specification as MainStreamSpecification<Character>

        return mainSpecification.getResults()
    }
}