package jodelka.me.marvelcharacters.data.rdp.specification.realm.impl

import io.realm.Realm
import jodelka.me.marvelcharacters.data.rdp.specification.base.AllCharactersSpecification
import jodelka.me.marvelcharacters.data.rdp.specification.realm.RealmStreamSpecification
import jodelka.me.marvelcharacters.viper.entity.Character
import rx.Observable
import java.util.*

/**
 * Created by jjodelka on 28/01/2017.
 */

class AllCharactersRealmSpecification(var offset: Int = 0, var limit: Int = 20) :
        RealmStreamSpecification<Character>, AllCharactersSpecification {

    override fun getResults(realm: Realm): Observable<Character> {
        // TODO: This method is a little slowly, improve that plox
        val observable = realm.where(Character::class.java).findAll().sort("name").asObservable()
        val charactersList: ArrayList<Character> = ArrayList()
        charactersList.subList(offset, limit)
        observable.subscribe { it -> charactersList.add(realm.copyFromRealm(it).first()) }
        return Observable.from(charactersList)
    }
}