package jodelka.me.marvelcharacters.data.rdp.specification.retrofit

import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import retrofit2.Retrofit
import rx.Observable

/**
 * Created by jjodelka on 24/01/2017.
 */

interface RetrofitStreamSpecification<T> : StreamSpecification<T> {

    fun getResults(retrofit: Retrofit): Observable<T>
}