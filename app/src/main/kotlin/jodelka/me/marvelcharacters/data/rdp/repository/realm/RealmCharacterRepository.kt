package jodelka.me.marvelcharacters.data.rdp.repository.realm

import io.realm.Realm
import jodelka.me.marvelcharacters.data.rdp.repository.Repository
import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import jodelka.me.marvelcharacters.data.rdp.specification.realm.RealmStreamSpecification
import jodelka.me.marvelcharacters.viper.entity.Character
import rx.Observable

/**
 * Created by jjodelka on 28/01/2017.
 */

class RealmCharacterRepository : Repository<Character> {

    private val realm: Realm
        get() {
            return Realm.getDefaultInstance()
        }

    override fun add(item: Character) {
        realm.executeTransactionAsync { realm -> realm.copyToRealmOrUpdate(item) }
    }

    override fun add(items: Observable<Character>) {
        items.subscribe { it -> add(it) }
    }

    override fun update(item: Character) {
        add(item)
    }

    override fun remove(item: Character) {
        val results = realm.where(Character::class.java).equalTo("id", item.id).findAll()
        realm.executeTransactionAsync { realm -> results.deleteAllFromRealm() }
    }

    override fun streamQuery(specification: StreamSpecification<Character>): Observable<Character> {
        val realmSpecification: RealmStreamSpecification<Character>
                = specification as RealmStreamSpecification<Character>

        return realmSpecification.getResults(realm)
    }
}