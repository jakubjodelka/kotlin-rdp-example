package jodelka.me.marvelcharacters.data.rdp.specification.base

import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import jodelka.me.marvelcharacters.viper.entity.Character

/**
 * Created by jjodelka on 24/01/2017.
 */

interface AllCharactersSpecification : StreamSpecification<Character>