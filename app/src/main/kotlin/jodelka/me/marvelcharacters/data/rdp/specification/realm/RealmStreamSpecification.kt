package jodelka.me.marvelcharacters.data.rdp.specification.realm

import io.realm.Realm
import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import rx.Observable

/**
 * Created by jjodelka on 28/01/2017.
 */

interface RealmStreamSpecification<T> : StreamSpecification<T> {

    fun getResults(realm: Realm): Observable<T>
}