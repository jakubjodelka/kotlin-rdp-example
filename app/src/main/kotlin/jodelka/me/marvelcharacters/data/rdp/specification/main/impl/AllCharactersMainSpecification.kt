package jodelka.me.marvelcharacters.data.rdp.specification.main.impl

import jodelka.me.marvelcharacters.data.rdp.specification.main.MainStreamSpecification
import jodelka.me.marvelcharacters.di.DIProvider
import jodelka.me.marvelcharacters.viper.entity.Character
import rx.Observable
import rx.schedulers.Schedulers

/**
 * Created by jjodelka on 29/01/2017.
 */

class AllCharactersMainSpecification(var offset: Int = 0, var limit: Int = 20) :
        MainStreamSpecification<Character> {

    override fun getResults(): Observable<Character> {
        val characterRealmRepository = DIProvider.repositoryComponent!!
                .provideRealmCharacterRepository()
        val characterRetrofitRepository = DIProvider.repositoryComponent!!
                .provideRetrofitCharacterRepository()

        characterRetrofitRepository.streamQuery(DIProvider.retrofitSpecificationComponent!!
                .provideAllCharactersSpecification())
                .subscribeOn(Schedulers.io())
                .subscribe { it -> characterRealmRepository.add(it) }

        return characterRealmRepository.streamQuery(DIProvider.realmSpecificationComponent!!
                .provideAllCharactersSpecification())
    }
}