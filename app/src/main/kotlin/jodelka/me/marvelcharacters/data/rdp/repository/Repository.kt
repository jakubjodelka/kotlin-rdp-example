package jodelka.me.marvelcharacters.data.rdp.repository

import jodelka.me.marvelcharacters.data.rdp.specification.StreamSpecification
import rx.Observable

/**
 * Created by jjodelka on 24/01/2017.
 */

interface Repository<T> {

    fun add(item: T)

    fun add(item: Observable<T>)

    fun update(item: T)

    fun remove(item: T)

    fun streamQuery(specification: StreamSpecification<T>): rx.Observable<T>
}