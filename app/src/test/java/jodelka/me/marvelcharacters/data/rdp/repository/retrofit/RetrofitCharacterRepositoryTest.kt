package jodelka.me.marvelcharacters.data.rdp.repository.retrofit

import com.orhanobut.logger.Logger
import jodelka.me.marvelcharacters.data.rdp.specification.retrofit.impl.AllCharactersRetrofitSpecification
import org.junit.Assert
import org.junit.Test

/**
 * Created by jjodelka on 24/01/2017.
 */

class RetrofitCharacterRepositoryTest {

    @Test
    fun getAllCharactersTest() {
        var charactersCount : Int = 0

        RetrofitCharacterRepository()
                .streamQuery(AllCharactersRetrofitSpecification())
                .subscribe(
                        { charactersCount++ },
                        { it -> it.printStackTrace() },
                        { Logger.d("onComplete") })

        Assert.assertEquals(20, charactersCount)
    }
}